package util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import domain.ApplicationException;

import java.io.IOException;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Helper {
    public static final Logger LOGGER = Logger.getLogger("Helper");
    public static final String JSON_CREATION_ERROR_MESSAGE = "Unable to create JSON";

    public static String dataToJson(Object data) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            StringWriter sw = new StringWriter();
            mapper.writeValue(sw, data);
            return sw.toString();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, JSON_CREATION_ERROR_MESSAGE);
            throw new ApplicationException(JSON_CREATION_ERROR_MESSAGE, e);
        }
    }
}
