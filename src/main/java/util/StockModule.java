package util;

import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Scopes;
import service.*;

public class StockModule implements Module {
    @Override
    public void configure(Binder binder) {
        binder.bind(SettingsService.class).to(SettingsServiceBean.class).in(Scopes.SINGLETON);
        binder.bind(CacheService.class).to(CacheServiceBean.class);
        binder.bind(StockParserService.class).to(StockParserServiceBean.class);
        binder.bind(StockService.class).to(StockServiceBean.class);
    }
}
