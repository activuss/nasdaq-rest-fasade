package domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Stock {
    private String name;
    private String cost;
    private String industry;
}
