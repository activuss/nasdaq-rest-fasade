package client;

import com.google.inject.Guice;
import com.google.inject.Injector;
import service.StockService;
import util.StockModule;

import static spark.Spark.get;

public class NasdaqStockResource {

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new StockModule());
        StockService stockService = injector.getInstance(StockService.class);

        get("/*", (req, res) -> stockService.getStockInfo(req));
    }
}
