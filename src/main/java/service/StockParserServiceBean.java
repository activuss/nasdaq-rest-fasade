package service;

import domain.ApplicationException;
import domain.Stock;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StockParserServiceBean implements StockParserService {
    public static final String PARSING_ERROR_MESSAGE = "Unable to parse ";
    public static final Logger LOGGER = Logger.getLogger("StockParserServiceBean");

    @Override
    public Optional<Stock> parseStock(String url) {
        LOGGER.info("Parsing data from " + url);

        try {
            Document doc = Jsoup.connect(url).get();

            if (doc.select(".notTradingIPO").text().isEmpty()) {
                return Optional.of(Stock.builder()
                                           .cost(doc.select(".qwidget-dollar").first().text())
                                           .name(doc.select(".qbreadcrumb b").text())
                                           .industry(doc.select("#qbar_sectorLabel a").text())
                                           .build());
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, PARSING_ERROR_MESSAGE + url, e.getCause());
            throw new ApplicationException(PARSING_ERROR_MESSAGE + url, e);
        }
        return Optional.empty();
    }
}
