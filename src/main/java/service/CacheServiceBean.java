package service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.inject.Inject;
import domain.Stock;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static domain.Setting.STOCK_TTL;

public class CacheServiceBean implements CacheService {
    private static final Logger LOGGER = Logger.getLogger("CacheService");

    protected Cache<String, Stock> storage;

    @Inject
    public CacheServiceBean(SettingsService settingsService) {
        storage = CacheBuilder.newBuilder()
                          .expireAfterWrite(Integer.valueOf(settingsService.getSetting(STOCK_TTL.name())), TimeUnit.SECONDS)
                          .build();
    }

    @Override
    public void cache(String name, Stock stock) {
        LOGGER.info("Caching value for stock " + name.toUpperCase());
        storage.put(name, stock);
    }

    @Override
    public Stock retrieve(String name) {
        LOGGER.info("Retrieving cached value for stock " + name.toUpperCase());
        return storage.getIfPresent(name);
    }
}
