package service;

import domain.Stock;

public interface CacheService {
    void cache(String name, Stock stock);

    Stock retrieve(String name);
}
