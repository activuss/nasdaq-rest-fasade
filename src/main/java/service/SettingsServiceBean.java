package service;

import com.google.common.collect.Maps;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import static domain.Setting.STOCK_SERVICE;
import static domain.Setting.STOCK_TTL;

public class SettingsServiceBean implements SettingsService {
    private static final Logger LOGGER = Logger.getLogger("SettingsService");

    private Map<String, String> settings = readSettings();

    public Map<String, String> readSettings() {
        Properties properties = new Properties();
        try {
            properties.load(SettingsServiceBean.class.getClassLoader().getResourceAsStream("settings.properties"));
            LOGGER.info("Settings were loaded from file.");
            return Maps.fromProperties(properties);

        } catch (IOException e) {
            LOGGER.info("Unable to load settings from file. Using default properties.");
        }
        return getDefaultProperties();
    }

    @Override
    public String getSetting(String setting) {
        return settings.get(setting);
    }

    private Map<String, String> getDefaultProperties() {
        Map<String, String> defaultProperties = new HashMap<>();
        defaultProperties.put(STOCK_TTL.name(), "60");
        defaultProperties.put(STOCK_SERVICE.name(), "http://www.nasdaq.com/symbol/");

        return defaultProperties;
    }
}
