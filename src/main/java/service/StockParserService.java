package service;

import domain.Stock;

import java.util.Optional;

public interface StockParserService {
    Optional<Stock> parseStock(String url);
}
