package service;

import com.google.inject.Inject;
import domain.ApplicationException;
import domain.Stock;
import spark.Request;

import java.util.Optional;
import java.util.logging.Logger;

import static domain.Setting.STOCK_SERVICE;
import static util.Helper.dataToJson;

public class StockServiceBean implements StockService {
    public static final String NOT_TRADING_IPO = "This stock is non trading IPO.";
    public static final String APP_ERROR = "Unable to get info regarding this stock.";
    public static final String STOCK_NOT_REQUESTED = "You should specify stock name in the URL";
    private static final Logger LOGGER = Logger.getLogger("StockService");
    private CacheService cache;
    private SettingsService settingsService;
    private StockParserService parserService;

    @Inject
    public StockServiceBean(SettingsService settingsService, CacheService cache, StockParserService parserService) {
        this.settingsService = settingsService;
        this.cache = cache;
        this.parserService = parserService;
    }

    @Override
    public String getStockInfo(Request request) {
        String input = request.uri();

        if (input.length() == 1) {
            return dataToJson(STOCK_NOT_REQUESTED);
        }

        String stockName = input.substring(1);

        try {
            String stockUrl = settingsService.getSetting(STOCK_SERVICE.name()) + stockName;

            Stock cachedValue = cache.retrieve(stockName);
            if (cachedValue != null) {
                return dataToJson(cachedValue);
            } else {
                LOGGER.info("Unable to fetch fresh cached data.");
                Optional<Stock> stock = parserService.parseStock(stockUrl);
                if (stock.isPresent()) {
                    cache.cache(stockName, stock.get());
                    return dataToJson(stock.get());
                } else {
                    LOGGER.info("Requested stock isn't exists: " + stockName.toUpperCase());
                    return dataToJson(NOT_TRADING_IPO);
                }
            }
        } catch (ApplicationException e) {
            return dataToJson(APP_ERROR);
        }
    }
}
