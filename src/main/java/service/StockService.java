package service;

import spark.Request;

public interface StockService {
    String getStockInfo(Request request);
}
