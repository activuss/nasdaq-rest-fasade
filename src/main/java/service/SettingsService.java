package service;

public interface SettingsService {
    String getSetting(String setting);
}
