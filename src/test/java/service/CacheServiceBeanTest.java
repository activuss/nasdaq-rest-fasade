package service;

import domain.Setting;
import domain.Stock;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CacheServiceBeanTest {
    private static final String STOCK_TTL = "3";
    private static final String STOCK_NAME = "Stock";

    private CacheServiceBean cacheService;
    private Stock stock;

    @Before
    public void setUp() {
        SettingsService settingsService = mock(SettingsService.class);
        when(settingsService.getSetting(Setting.STOCK_TTL.name())).thenReturn(STOCK_TTL);

        cacheService = new CacheServiceBean(settingsService);

        stock = Stock.builder()
                        .name(STOCK_NAME)
                        .industry("Industry")
                        .cost("123")
                        .build();
    }

    @Test
    public void cacheTest() {
        assertEquals(0L, cacheService.storage.size());
        cacheService.cache(STOCK_NAME, stock);
        assertEquals(1L, cacheService.storage.size());
    }

    @Test
    public void retrieveTest() {
        cacheService.cache(STOCK_NAME, stock);
        assertEquals(1L, cacheService.storage.size());
        assertEquals(stock, cacheService.retrieve(STOCK_NAME));
    }

    @Test
    @SneakyThrows
    public void retrieveAfterExpirationTest() throws InterruptedException {
        cacheService.cache(STOCK_NAME, stock);
        assertEquals(1L, cacheService.storage.size());
        assertEquals(stock, cacheService.retrieve(STOCK_NAME));
        TimeUnit.SECONDS.sleep(3L);
        assertNotEquals(stock, cacheService.retrieve(STOCK_NAME));
    }
}
