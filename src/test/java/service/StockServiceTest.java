package service;

import domain.Setting;
import domain.Stock;
import org.junit.Before;
import org.junit.Test;
import spark.Request;

import java.util.Optional;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.*;
import static service.StockServiceBean.STOCK_NOT_REQUESTED;
import static util.Helper.dataToJson;

public class StockServiceTest {
    public static final String STOCK_SERVICE = "some.service/";
    public static final String STOCK_TTL = "1";
    private static final String STOCK_NAME = "stock";
    private StockService stockService;
    private CacheService cacheService;
    private Stock stockFromCache;
    private StockParserService parserService;


    @Before
    public void setUp() {
        cacheService = mock(CacheService.class);
        parserService = mock(StockParserService.class);
        SettingsService settingsService = mock(SettingsService.class);
        stockService = new StockServiceBean(settingsService, cacheService, parserService);

        stockFromCache = Stock.builder()
                                 .name(STOCK_NAME)
                                 .cost("111")
                                 .industry("industry")
                                 .build();

        when(settingsService.getSetting(Setting.STOCK_TTL.name())).thenReturn(STOCK_TTL);
        when(settingsService.getSetting(Setting.STOCK_SERVICE.name())).thenReturn(STOCK_SERVICE);
    }

    @Test
    public void getStockInfoStockNotRequestedTest() {
        Request request = mock(Request.class);
        when(request.uri()).thenReturn("/");

        String stockInfo = stockService.getStockInfo(request);

        assertTrue(stockInfo.contains(STOCK_NOT_REQUESTED));
    }

    @Test
    public void getStockInfoFreshStockFoundInCacheTest() {
        Request request = mock(Request.class);
        when(request.uri()).thenReturn("/" + STOCK_NAME);

        when(cacheService.retrieve(STOCK_NAME)).thenReturn(stockFromCache);

        String serviceResponse = stockService.getStockInfo(request);

        assertEquals(dataToJson(stockFromCache), serviceResponse);

        verify(cacheService).retrieve(STOCK_NAME);
        verifyNoMoreInteractions(cacheService);
    }

    @Test
    public void getStockInfoFreshStockNotFoundInCacheTest() {
        Request request = mock(Request.class);
        when(request.uri()).thenReturn("/" + STOCK_NAME);

        when(cacheService.retrieve(STOCK_NAME)).thenReturn(null);
        when(parserService.parseStock(anyString())).thenReturn(Optional.of(stockFromCache));

        String serviceResponse = stockService.getStockInfo(request);

        assertEquals(dataToJson(stockFromCache), serviceResponse);

        verify(cacheService).cache(STOCK_NAME, stockFromCache);
    }

    @Test
    public void getStockInfoNonTradingIPOTest() {
        Request request = mock(Request.class);
        when(request.uri()).thenReturn("/" + STOCK_NAME);

        when(cacheService.retrieve(STOCK_NAME)).thenReturn(null);
        when(parserService.parseStock(anyString())).thenReturn(Optional.empty());

        String serviceResponse = stockService.getStockInfo(request);

        assertTrue(serviceResponse.contains(StockServiceBean.NOT_TRADING_IPO));
    }

}
