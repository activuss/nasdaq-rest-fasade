Simple rest fasade for www.nasdaq.com intended for retrieving some stock info (stock name, cost, industry). REST infrastructure provided by Spark framework: http://sparkjava.com/

For runnung use:

mvn compile && mvn exec:java

Example of usage:

GET http://localhost:4567/orcl

Response:
{
name: "ORCL",
cost: "$37.21",
industry: "Technology"
}
